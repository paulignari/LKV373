#!/usr/bin/python2

#Packet sniffer in python
#For Linux - Sniffs all incoming and outgoing packets :)
#Silver Moon (m00n.silv3r@gmail.com)
#modified by danman
#modified by Pauli


import socket, sys
from struct import *
import struct
import binascii
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument ("dev", help="device on which stream is received")
parser.add_argument ("tx", help="transmitter number")
args=parser.parse_args()

#create a AF_PACKET type raw socket (thats basically packet level)
#define ETH_P_ALL    0x0003          /* Every packet (be careful!!!) */
try:
    s = socket.socket( socket.AF_PACKET , socket.SOCK_RAW , socket.ntohs(0x0003))
except (socket.error, msg):
    print ("Socket could not be created. Error Code : {} Message {}".format(str(msg[0]),msg[1]))
    sys.exit()

# Open transmit channel
# play stream with
# vlc --demux=mjpeg --network-caching=20  udp://@:10001/
try:
    t = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
except (socket.error, msg):
    print ("Socket could not be created. Error Code : {} Message {}".format(str(msg[0]),msg[1]))
    sys.exit()

iface=args.dev
tx=int(args.tx)
sender = binascii.a2b_hex("000b780000{:02}".format(tx))
notopen=1

# put interface in promisous mode
os.system("ip link set {} promisc on".format(iface))
# set MTU to 9000
os.system("ip link set {} mtu 9000".format(iface))

# receive a packet
while True:
    packet = s.recvfrom(65565)

    #packet string from tuple
    packet = packet[0]

    #parse ethernet header
    eth_length = 14

    eth_header = packet[:eth_length]
    eth = unpack('!6s6sH' , eth_header)
    eth_protocol = socket.ntohs(eth[2])


    if (packet[6:12] == sender) & (eth_protocol == 8) :

        #Parse IP header
        #take first 20 characters for the ip header
        ip_header = packet[eth_length:20+eth_length]

        #now unpack them :)
        iph = unpack('!BBHHHBBH4s4s' , ip_header)

        version_ihl = iph[0]
        version = version_ihl >> 4
        ihl = version_ihl & 0xF

        iph_length = ihl * 4

        ttl = iph[5]
        protocol = iph[6]
        s_addr = socket.inet_ntoa(iph[8]);
        d_addr = socket.inet_ntoa(iph[9]);

        #UDP packets
        if protocol == 17 :
            u = iph_length + eth_length
            udph_length = 8
            udp_header = packet[u:u+8]

            #now unpack them :)
            udph = unpack('!HHHH' , udp_header)

            source_port = udph[0]
            dest_port = udph[1]
            length = udph[2]
            checksum = udph[3]

            #get data from the packet
            h_size = eth_length + iph_length + udph_length
            data = packet[h_size:]

            if (dest_port==2068):
              frame_n=ord(data[0])*256+ord(data[1])
              part=ord(data[2])*256+ord(data[3])
              if (part==0) : # & (notopen==1) :
                 notopen=0
              if notopen==0:
                 t.sendto(data[4:], ('127.0.0.1',10000+tx))

# leave promiscous mode
os.system("ip link set {} promisc off".format(iface))
