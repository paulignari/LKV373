#!/usr/bin/python

'''
LKV373 Heartbeat generator
By Pauli

Listen for incoming UDP packet on port 48689, display
transmitter infos and send back heartbeat packet

Can be used with any number of transmitters on the
same lan; they are numbered by the last octet of the
originating ip (require changing address on trasmitter).

Based on great work of
* danman https://danman.eu/blog/reverse-engineering-lenkeng-hdmi-over-ip-extender/
* toru173 https://github.com/toru173/Lenkeng373.git
'''

from struct import *
import socket
from time import *
import errno
import sys

# Consider tx lost after 2 seconds without heartbeat packets
timeOut=2
lastSeen = {}

s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.settimeout(0.1)
s.bind(('<broadcast>',48689))

print "\x1b[2J"
print "\x1b[0;0HTX Rec State       Signal format    Encoded format   Uptime"

heartbeat_data = '5446367a60020000000000030301002600000000000000'.decode('hex')

while True:

  for k in lastSeen.keys():
    delay=int(time() - lastSeen[k])
    if delay > timeOut :
      print "\x1b[{};0H\x1b[2K\x1b[101m{:2}\x1b[49m".format(k+1,k)

  try:
    data=s.recvfrom(1024)
  except socket.error as msg:
    if msg[0] == 'timed out':
      continue
    else:
      print ("Error: {}".format(msg[0]))
      sys.exit()

  o4 = int(data[1][0].split(".")[3])
  tx = o4
  lastSeen[tx] = int(time())
  signal_present = unpack('>B', data[0][27 : 28])[0]
  signal_width = unpack('>H', data[0][28 : 30])[0]
  signal_height = unpack('>H', data[0][30 : 32])[0]
  signal_fps = unpack('>H', data[0][32 : 34])[0]
  encoded_width = unpack('>H', data[0][34 : 36])[0]
  encoded_height = unpack('>H', data[0][36 : 38])[0]
  uptime = unpack('>L', data[0][40 : 44])[0] # IN MILLISECONDS
  receiver_present = unpack('>B', data[0][50 : 51])[0]
                    
  if signal_present == 3 :
    signal_present = "\x1b[102mSignal OK\x1b[49m" 
  else :
    signal_present = "\x1b[101mNo Signal\x1b[49m"
                    
  signal_fps = signal_fps / 10.0 # FPS correction, found by varying FPS at source.
  encoded_fps = signal_fps

  seconds = uptime / 1000
  minutes, seconds = divmod(seconds, 60)
  hours, minutes = divmod(minutes, 60)
  days, hours = divmod(hours, 24)

  print "\x1b[{};0H{:2} {}   {}   {:4}x{:4}@{:4}   {:4}x{:4}@{:4}   {:2}d {:02}:{:02}:{:02}".format(tx+1,tx, receiver_present,signal_present,signal_width,signal_height,signal_fps,encoded_width,encoded_height,encoded_fps,days, hours, minutes, seconds)

  s.sendto(heartbeat_data, data[1])
